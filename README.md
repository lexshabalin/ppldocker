# test-project

1. Что необходимо сделать?
   - OK Упаковать данный проект в контейнер. Размер конечного image не должен превышать 200МБ;
   - Написать gitlab-ci.yml который будет запускаться по коммиту в ветку testing-branch;
   - OK добавить git submodule с helm chart и shell скриптами;
   - Добавить в директорию .ci/ values для Helm и skaffold;
3. Вводные данные о приложении

Base domain: *.pnpl.tech

Поддерживает следующие ENV для конфигурации:


| env_name | default_value | description            | required |
| -------- | ------------- | ---------------------- | -------- |
| APP_HOST |               | default listen address | true     |
| APP_PORT |               | default listen port    | true     |
