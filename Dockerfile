#FROM python:3.12-rc-slim as builder
#RUN mkdir /build
#WORKDIR /build
#COPY . . /build/
#RUN python -m pip install --no-cache-dir --requirement requirements.txt

#FROM nginx:1.22-alpine
#COPY --from=builder /build/* /var/www/myapp

#EXPOSE 8080

#CMD ["nginx", "-g", "daemon off;"]

FROM python:3.12-rc-slim
WORKDIR "/"

COPY requirements.txt .
RUN python -m pip install --no-cache-dir --requirement requirements.txt

COPY main.py .

CMD ["python", "main.py"]
